package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.LureEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(LureEnchantment.class)
public class LureMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
