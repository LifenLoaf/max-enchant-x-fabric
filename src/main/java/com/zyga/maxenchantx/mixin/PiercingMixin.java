package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.PiercingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PiercingEnchantment.class)
public class PiercingMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
