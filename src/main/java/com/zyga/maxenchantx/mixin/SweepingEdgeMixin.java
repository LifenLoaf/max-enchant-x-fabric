package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.SweepingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(SweepingEnchantment.class)
public class SweepingEdgeMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
