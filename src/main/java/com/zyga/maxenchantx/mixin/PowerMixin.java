package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.PowerEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PowerEnchantment.class)
public class PowerMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
