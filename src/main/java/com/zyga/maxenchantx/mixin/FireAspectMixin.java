package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.FireAspectEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(FireAspectEnchantment.class)
public class FireAspectMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
