package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.PunchEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PunchEnchantment.class)
public class PunchMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
