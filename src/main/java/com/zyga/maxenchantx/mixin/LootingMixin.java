package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.LuckEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(LuckEnchantment.class)
public class LootingMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
