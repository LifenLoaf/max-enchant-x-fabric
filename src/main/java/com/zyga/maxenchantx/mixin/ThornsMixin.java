package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.ThornsEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(ThornsEnchantment.class)
public class ThornsMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
