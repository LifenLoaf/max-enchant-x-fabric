package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.DamageEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(DamageEnchantment.class)
public class SharpnessMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
