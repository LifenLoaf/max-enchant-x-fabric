package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.UnbreakingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(UnbreakingEnchantment.class)
public class UnbreakingMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
