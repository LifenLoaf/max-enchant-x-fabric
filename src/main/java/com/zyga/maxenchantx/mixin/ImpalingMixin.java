package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.ImpalingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(ImpalingEnchantment.class)
public class ImpalingMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
